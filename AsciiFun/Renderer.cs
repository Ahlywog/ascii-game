﻿using System.Collections.Generic;

namespace AsciiFun
{
    public class Renderer: Component
    {
        public bool Enabled { get; set; } = true;

        public bool isVisible { get; set; } = true;

        public bool ClearOnPreRender { get; set; } = true;

        public bool hasChanged { get; set; } = false;

        public List<Cell> Cells { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="height"></param>
        /// <param name="width"></param>
        public Renderer()
        {
            Cells = new List<Cell>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cells"></param>
        public void AddCells(params Cell[] cells)
        {
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i].Transform.gameObject = gameObject;
            }
            Cells.AddRange(cells);

            hasChanged = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearCells()
        {
            Cells.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnBecomeInvisible()
        {
            /*
                Filler!
            */
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnBecomeVisible()
        {
            /*
                Filler!
            */
        }

        /// <summary>
        /// 
        /// </summary>
        public void PreRender()
        {
            if (hasChanged)
            {
                BVH.RemoveCells(Cells.ToArray());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Render()
        {
            if (    Enabled == true &&
                    isVisible == true &&
                    hasChanged == true  )
            {
                BVH.AddCells(Cells.ToArray());

                hasChanged = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void PostRender()
        {
            /*
                Filler!
            */
        }
    }
}
