﻿using System.Runtime.InteropServices;

namespace AsciiFun
{
    [StructLayout(LayoutKind.Explicit)]
    public struct CHAR_UNION
    {
        [FieldOffset(0)]
        public char UnicodeChar;
        [FieldOffset(0)]
        public byte AsciiChar;
    }
}
