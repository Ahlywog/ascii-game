﻿using System.Windows;

namespace AsciiFun
{
    public class Transform : Component
    {
        private bool hasChanged = false;

        private Transform parent = null;

        private Point position;

        public bool HasChanged
        {
            get
            {
                if (hasChanged == true)
                {
                    hasChanged = false;
                    return true;
                }
                return false;
            }
        }

        public Point Position
        {
            get
            {
                return position;
            }
            set
            {
                if (value != position)
                {
                    hasChanged = true;
                    position = value;
                }
            }
        }

        public Transform Parent
        {
            get
            {
                return parent;
            }
            set
            {
                if (value != parent)
                {
                    hasChanged = true;
                    parent = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Transform(double x, double y)
        {
            Position = new Point(x, y);
            hasChanged = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        public Transform(Point position, Transform parent = null)
        {
            this.Position = position;
            this.Parent = parent;
            hasChanged = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public Transform Root
        {
            get
            {
                Transform root = Parent;

                while (root != null)
                {
                    root = root.Parent;
                }

                return root;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Point position)
        {
            this.Position = position;
        }
    }
}
