﻿using System;
using System.Collections.Generic;

namespace AsciiFun
{
    public class Cell
    {
        private ConsoleColor backGround = ConsoleColor.Black;
        private ConsoleColor foreGround = ConsoleColor.White;

        private char character;

        private CHAR_INFO charInfo = new CHAR_INFO();

        public CellLayer Layer { get; set; }

        public TransparentBackground TransparentBackground { get; set; }

        public Transform Transform { get; set; }

        public CHAR_INFO @CharInfo
        {
            get
            {
                return charInfo;
            }
            set
            {
                charInfo = value;
            }
        }

        public ConsoleColor BackGround
        {
            get
            {
                return backGround;
            }
            set
            {
                backGround = value;
                charInfo.Attributes = (short)(((int)backGround << 4) + foreGround);
            }
        }

        public ConsoleColor ForeGround
        {
            get
            {
                return foreGround;
            }
            set
            {
                foreGround = value;
                charInfo.Attributes = (short)(((int)backGround << 4) + foreGround);
            }
        }

        public char Character
        {
            get
            {
                return character;
            }
            set
            {
                character = value;
                charInfo.Char = new CHAR_UNION()
                {
                    UnicodeChar = Character,
                    AsciiChar = (byte)Character
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="layer"></param>
        /// <param name="character"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="backGround"></param>
        /// <param name="foreGround"></param>
        /// <param name="transparentBackground"></param>
        public Cell(    CellLayer layer = CellLayer.BACKGROUND, 
                        char character = new char(),
                        float x = 0f,
                        float y = 0f,
                        ConsoleColor backGround = ConsoleColor.DarkGreen,
                        ConsoleColor foreGround = ConsoleColor.White,
                        TransparentBackground transparentBackground = TransparentBackground.FALSE)
        {
            Layer = layer;
            BackGround = backGround;
            ForeGround = foreGround;
            Character = character;
            TransparentBackground = transparentBackground;

            Transform = new Transform(x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        public Cell(Cell cell)
        {
            Layer = cell.Layer;
            BackGround = cell.backGround;
            ForeGround = cell.foreGround;
            Character = cell.charInfo.Char.UnicodeChar;
            TransparentBackground = cell.TransparentBackground;

            Transform = new Transform(  cell.Transform.Position.X,
                                        cell.Transform.Position.Y   );
        }
    }
}
