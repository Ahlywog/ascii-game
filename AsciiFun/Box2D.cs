﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiFun
{
    public class Box2D
    {
        public Vector2D Position;
        public Vector2D Size;

        public double Top
        {
            get
            {
                return Position.Y;
            }
        }

        public double Bottom
        {
            get
            {
                return Position.Y - Size.Y;
            }
        }

        public double Left
        {
            get
            {
                return Position.X;
            }
        }

        public double Right
        {
            get
            {
                return Position.X + Size.X;
            }
        }

        public bool Contains(Box2D box)
        {
            return  Left < box.Left &&
                    Right > box.Right &&
                    Top > box.Top &&
                    Bottom < box.Bottom;
        }

        public bool Contains(Vector2D vector)
        {
            return  Left < vector.X &&
                    Right > vector.X &&
                    Top > vector.Y &&
                    Bottom < vector.Y;
        }

        //public bool Intersects(Box2D box)
        //{
        //    //  X1, Y1, X2, Y2 : the coordinates of the points of the first rectangle (with X1 < X2 and Y1 < Y2)
        //    //  X1', Y1', X2', Y2' : the coordinates of the points of the second rectangle (with X1' < X2' and Y1' < Y2')
        //    //  (X2' >= X1 && X1' <= X2) && (Y2' >= Y1 && Y1' <= Y2)

        //    return  (   box.Left >= Left && box.Right <= Right  ) &&
        //            (   box.Top );
        //}
    }
}
