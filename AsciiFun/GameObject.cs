﻿using System.Collections.Generic;
using System.Linq;

namespace AsciiFun
{
    public class GameObject: Object
    {
        public List<Component> Components { get; private set; } = new List<Component>();

        public GameObject gameObject { get; set; } = null; // Parent GameObject

        public Transform transform { get; set; } = null; // Transform attached to this GameObject

        public string Tag { get; set; } = string.Empty;

        public bool Active { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="components"></param>
        public GameObject(string name = "", params Component[] components)
        {
            Name = name;

            this.Components.AddRange(components);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="active"></param>
        public void SetActive(bool active)
        {
            this.Active = active;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="components"></param>
        public void AddComponents(params Component[] components)
        {
            for (var i = 0; i < components.Length; i++)
            {
                components[i].gameObject = this;
            }

            this.Components.AddRange(components);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        public void SendMessage(string methodName, params object[] args)
        {
            if (Active)
            {
                var components = GetComponents<Behaviour>();

                for (int i = 0; i < components.Count(); i++)
                {
                    components[i].GetType().GetMethod(methodName)?.Invoke(components[i], new object[] { args });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetComponent<T>() where T : Component
        {
            return Components.Where(x => x is T).Cast<T>().First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T[] GetComponents<T>() where T : Component
        {
            return Components.Where(x => x is T).Cast<T>().ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static GameObject Find(string name)
        {
            return FindObjectsOfType<GameObject>()?.Where(x => x.Name == name).First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static GameObject[] FindGameObjectsWithTag(string tag)
        {
            return FindObjectsOfType<GameObject>()?.Where(x => x.Tag == tag).ToArray();
        }
    }
}
