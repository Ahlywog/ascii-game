﻿using AsciiFun.Scripts;
using System;
using System.Text;
using System.Windows;

namespace AsciiFun
{
    class Program
    {
        public static Random rnd = new Random();

        static void Main(string[] args)
        {
            TimeStep timeStep = new TimeStep(true);
            timeStep.SetStep(1 / 60f);

            TimeStep frameLimiter = new TimeStep(true);
            frameLimiter.SetStep(1 / 2000f);            

            GraphicsEngine.SetBufferSize(100, 50);
            GraphicsEngine.SetConsoleBufferSize(100, 50);
            GraphicsEngine.SetOutputEncoding(Encoding.Default);

            BVH.Init(1, 1, 1073741824, 1073741824);

            Cell clearCell = new Cell(  CellLayer.BACKGROUND,
                                        '\0',
                                        0f,
                                        0f,
                                        ConsoleColor.DarkGreen,
                                        ConsoleColor.Black,
                                        TransparentBackground.FALSE );

            GraphicsEngine.SetClearBuffer(clearCell);


            // Camera

            Camera mainCamera = new Camera(100, 50, 50, 25)
            {
                Name = "Main Camera",
                transform = new Transform(50, 25),
                cameraOffset = new Point(50, 25),
                Active = true
            };

            // Test Objects

            for (int i = 0; i < 10000; i++)
            {
                GameObject testObject = new GameObject("Test Object")
                {
                    transform = new Transform(50, 25),
                    Active = true
                };

                Cell testCell = new Cell(   GetRandomEnumValue<CellLayer>(),
                                            'X',
                                            0,
                                            0,
                                            GetRandomEnumValue<ConsoleColor>(),
                                            GetRandomEnumValue<ConsoleColor>(),
                                            GetRandomEnumValue<TransparentBackground>()    )
                {
                    Transform = new Transform(0, 0)
                    {
                        Parent = testObject.transform
                    }
                };

                Renderer testRenderer = new Renderer()
                {
                    Name = "Test Object Renderer"
                };
                testRenderer.AddCells(testCell);

                CircularMotion circMo = new CircularMotion()
                {
                    angle = rnd.Next(0, 360),
                    center = new Point(50, 25),
                    radius = new Point( rnd.Next(10, 25),
                                        rnd.Next(6, 20) ),
                    rateOfRotation = rnd.Next(5, 30),
                };

                testObject.AddComponents(   testRenderer,
                                            circMo  );
            }

            // FPS Counter

            GameObject fpsObject = new GameObject("FPS Counter")
            {
                Name = "FPS Counter",
                transform = new Transform(-50, -25)
                {
                    Parent = mainCamera.transform
                },
                Active = true
            };

            Renderer fpsRenderer = new Renderer()
            {
                Name = "FPS Renderer",
            };

            FPS fpsCounter = new FPS()
            {
                GetFPS = GraphicsEngine.GetFPS,
                Name = "FPS Script",
                StringMask = "FPS: ###0.000 fps",
                BackgroundColor = ConsoleColor.DarkGreen,
                ForegroundColor = ConsoleColor.White,
                BackgroundTransparency = TransparentBackground.FALSE
            };

            fpsObject.AddComponents(    fpsCounter,
                                        fpsRenderer );

            // Object Counter

            GameObject objectCounter = new GameObject("Object Counter")
            {
                Name = "Object Counter",
                transform = new Transform(-50, -24)
                {
                    Parent = mainCamera.transform
                },
                Active = true
            };

            Renderer objectCounterRenderer = new Renderer()
            {
                Name = "Object Counter Renderer"
            };

            FPS objectCounterScript = new FPS()
            {
                GetFPS = GraphicsEngine.GetTotalObjectsDrawn,
                Name = "Object Counter Script",
                StringMask = "Objects: ######0 On Screen",
                BackgroundColor = ConsoleColor.DarkGreen,
                ForegroundColor = ConsoleColor.White,
                BackgroundTransparency = TransparentBackground.FALSE
            };

            objectCounter.AddComponents(    objectCounterRenderer,
                                            objectCounterScript  );

            // ====================================================== Main Process ==========================================================

            // Setup anything that needs setting up before we start the main loop.
            var behaviours = Object.FindObjectsOfType<Behaviour>();

            for (var i = 0; i < behaviours.Length; i++)
            {
                behaviours[i].Start();
            }

            // Main game loop
            do
            {
                // Inner game loop.  Will probably have to change this to account 
                // for key press event handling.
                while (!Console.KeyAvailable)
                {
                    timeStep.Tick();
                    frameLimiter.Tick();

                    behaviours = Object.FindObjectsOfType<Behaviour>();

                    // Non-fixed update using current delta time
                    for (var i = 0; i < behaviours.Length; i++)
                    {
                        if (behaviours[i].Enabled)
                        {
                            behaviours[i].Update(timeStep.Delta);
                        }
                    }

                    // Fixed update using time step
                    while (timeStep.Ready)
                    {
                        for (var i = 0; i < behaviours.Length; i++)
                        {
                            if (behaviours[i].Enabled)
                            {
                                behaviours[i].FixedUpdate(timeStep.Step);
                            }
                        }
                    }

                    //  Frame limiting to reduce wasted prerender/render/postrencder cycles
                    if (frameLimiter.Ready)
                    {
                        var renderers = Object.FindObjectsOfType<Renderer>();

                        // Do whatever steps the renderers need to do before
                        //  rendering the display.
                        for (var i = 0; i < renderers.Length; i++)
                        {
                            if (renderers[i].Enabled)
                            {
                                renderers[i].PreRender();
                            }
                        }

                        // Render the display.  
                        for (var i = 0; i < renderers.Length; i++)
                        {
                            if (renderers[i].Enabled)
                            {
                                renderers[i].Render();
                            }
                        }

                        // Currently the code LOOKS for multiple cameras but I don't know
                        // what to do with multiple.  For now we'll only use one.
                        var cameras = Object.FindObjectsOfType<Camera>();

                        for (int i = 0; i < cameras.Length; i++)
                        {
                            if (cameras[i].Active)
                            {
                                cameras[i].Display();
                            }
                        }

                        // Do whatever steps the renderers need to do after
                        // rendering to the display.
                        for (var i = 0; i < renderers.Length; i++)
                        {
                            if (renderers[i].Enabled)
                            {
                                renderers[i].PostRender();
                            }
                        }
                    }
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            GraphicsEngine.Exit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetRandomEnumValue<T>()
        {
            var values = Enum.GetValues(typeof(T));

            return (T)values.GetValue(rnd.Next(values.Length));
        }
    }
}
