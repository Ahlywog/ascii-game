﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiFun
{
    public class Behaviour : Component
    {
        public bool Enabled { get; set; } = true;

        public virtual void OnKeyDown(ConsoleKey[] keys)
        {
            // Do all the things!
        }

        public virtual void OnKeyHold(ConsoleKey[] keys)
        {
            // Do all the things!
        }

        public virtual void OnKeyUp(ConsoleKey[] keys)
        {
            // Do all the things!
        }

        public virtual void OnBecomeVisible(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnBecomeInvisible(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnDestroy(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnDisable(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnGUI(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnPreRender(params object[] args)
        {
            // Do all the things!
        }

        public virtual void OnPostRender(params object[] args)
        {
            // Do all the things!
        }

        public virtual void Awake(params object[] args)
        {
            // Do all the things!
        }

        public virtual void Start()
        {
            // Do all the things!
        }


        public virtual void Update(double step)
        {
            // Do all the things!
        }

        public virtual void FixedUpdate(double step)
        {
            // Do all the things!
        }
    }
}
