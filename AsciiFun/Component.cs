﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiFun
{
    public abstract class Component : Object
    {
        public GameObject gameObject { get; set; } = null;  // Parent GameObject

        public string tag { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="otherTag"></param>
        /// <returns></returns>
        public bool CompareTag(string otherTag)
        {
            return tag == otherTag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        public void SendMessage(string methodName, params object[] args)
        {
            gameObject?.SendMessage(methodName, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetComponent<T>() where T : Component
        {
            return gameObject?.GetComponent<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T[] GetComponents<T>() where T : Component
        {
            return gameObject?.GetComponents<T>();
        }
    }
}
