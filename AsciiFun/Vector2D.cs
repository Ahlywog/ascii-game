﻿using System;

namespace AsciiFun
{
    public class Vector2D : IEquatable<Vector2D>
    {
        public static Vector2D Zero = new Vector2D(0f, 0f);

        public static Vector2D Down = new Vector2D(0f, -1f);

        public static Vector2D Left = new Vector2D(-1f, 0f);

        public static Vector2D One = new Vector2D(1f, 1f);

        public static Vector2D Right = new Vector2D(1f, 0f);

        public static Vector2D Up = new Vector2D(0f, 1f);

        public double X { get; set; } 

        public double Y { get; set; }

        public double Magnitude()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public double SqrMagnitude()
        {
            return (X * X + Y * Y);
        }

        public Vector2D Normalized
        {
            get
            {
                var magnitude = Magnitude();
                return new Vector2D( X / magnitude,
                                    Y / magnitude   );
            }
        }

        public Vector2D(double x = 0f, double y = 0f)
        {
            X = x;
            Y = y;
        }

        public static Vector2D operator !(Vector2D vector)
        {
            return new Vector2D()
            {
                X = -vector.X,
                Y = -vector.Y
            };
        }

        public static Vector2D operator -(Vector2D a, Vector2D b)
        {
            return new Vector2D()
            {
                X = a.X - b.X,
                Y = a.Y - b.Y
            };
        }

        public static Vector2D operator +(Vector2D a, Vector2D b)
        {
            return new Vector2D()
            {
                X = a.X + b.X,
                Y = a.Y + b.Y
            };
        }

        public static Vector2D operator /(Vector2D vector, double denominator)
        {
            return new Vector2D()
            {
                X = vector.X / denominator,
                Y = vector.Y / denominator
            };
        }

        public static Vector2D operator *(Vector2D vector, double multiplier)
        {
            return new Vector2D()
            {
                X = vector.X * multiplier,
                Y = vector.Y * multiplier
            };
        }

        public static bool operator ==(Vector2D a, Vector2D b)
        {
            return (a.X == b.X && a.Y == b.Y);
        }

        public static bool operator !=(Vector2D a, Vector2D b)
        {
            return (a.X != b.X || a.Y != b.Y);
        }

        public static double Distance(Vector2D a, Vector2D b)
        {
            return (a - b).Magnitude();
        }

        public static double SqrDistance (Vector2D a, Vector2D b)
        {
            return (a - b).SqrMagnitude();
        }

        public static double Dot(Vector2D a, Vector2D b)
        {
            return (a.X * b.X) + (a.Y * b.Y);
        }

        public static Vector2D Lerp(Vector2D a, Vector2D b, float interpolation)
        {
            interpolation = Mathf.Clamp(0f, 1f, interpolation);

            Vector2D c = b - a;

            return c * interpolation;
        }

        public static bool InBounds(Vector2D position, Vector2D TopLeft, Vector2D BottomRight)
        {
            return (    position.X >= TopLeft.X &&
                        position.X <= BottomRight.X &&
                        position.Y <= TopLeft.Y &&
                        position.Y >= BottomRight.Y );
        }

        public static Vector2D ConvertPositionToCameraSpace(Vector2D position, Vector2D cameraPosition)
        {
            return position + Vector2D.Zero - cameraPosition;
        }


        public static Vector2D ConvertFromWorldToArraySpace(Vector2D Position, Vector2D ArrayPosition)
        {
            return Position + Vector2D.Zero - ArrayPosition;
        }

        public bool Equals(Vector2D other)
        {
            return (X == other.X && Y == other.Y);
        }

        public override bool Equals(object obj)
        {
            Vector2D vector = (Vector2D)obj;

            return (vector != null) && this.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("X: {0}, Y: {1}", X, Y);
        }
    }
}