﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AsciiFun
{
    public static class Point_ExtensionMethods
    {
        public static Point Add (this Point A, Point B)
        {
            return new Point(A.X + B.X, A.Y + B.Y);
        }

        public static Point Subtract(this Point A, Point B)
        {
            return new Point(A.X - B.X, A.Y - B.Y);
        }

        public static double Length(this Point A)
        {
            return Math.Sqrt(Math.Pow(A.X, 2) + Math.Pow(A.Y, 2));
        }

        public static double LengthSqr(this Point A)
        {
            return Math.Pow(A.X, 2) + Math.Pow(A.Y, 2);
        }

        public static Point ConvertPositionToCameraSpace(this Point position, Point cameraPosition)
        {
            return position.Subtract(cameraPosition);
        }

        public static double Distance(this Point A, Point B)
        {
            var point = new Point(A.X - B.X, A.Y - B.Y);
            return point.Length();
        }

        public static double DistanceSqr(this Point A, Point B)
        {
            var point = new Point(A.X - B.X, A.Y - B.Y);
            return point.LengthSqr();
        }

        public static double Dot(this Point A, Point B)
        {
            return (A.X * B.X) + (A.Y * B.Y);
        }

        public static int CompareTo(this Point A, Point B)
        {
            return A.Length().CompareTo(B.Length());
        }
    }
}
