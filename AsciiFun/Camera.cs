﻿using System.Windows;

namespace AsciiFun
{
    public class Camera : GameObject
    {
        public Point cameraOffset { get; set; }

        public Rect viewFrustrum { get; set; }

        public int screenBufferWidth { get; set; } = 0;
        public int screenBufferHeight { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewFrustrumWidth"></param>
        /// <param name="viewFrustrumHeight"></param>
        public Camera(  int viewFrustrumWidth,
                        int viewFrustrumHeight,
                        float x,
                        float y )
        {
            screenBufferWidth = viewFrustrumWidth;
            screenBufferHeight = viewFrustrumHeight;

            transform = new Transform(x, y);

            cameraOffset = new Point(   screenBufferWidth / 2f,
                                        screenBufferHeight / 2f );

            viewFrustrum = new Rect(    x - viewFrustrumWidth / 2f, 
                                        y - viewFrustrumHeight / 2f, 
                                        viewFrustrumWidth, 
                                        viewFrustrumHeight  );
    }

        /// <summary>
        /// 
        /// </summary>
        public void Display()
        {
            //GraphicsEngine.Render(transform.Position, cameraOffset);
            GraphicsEngine.Render(this);
        }
    }
}
