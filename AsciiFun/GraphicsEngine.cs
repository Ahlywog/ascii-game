﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Text;

namespace AsciiFun
{
    public enum CellLayer { BACKGROUND, TERRAIN, PLAYER, EFFECTS, FOREGROUND, UI }

    public enum TransparentBackground { TRUE, FALSE }

    public class GraphicsEngine: NativeMethods
    {
        private static GraphicsEngine _instance = null;

        private SafeFileHandle _handle = null;

        private CHAR_INFO[,] _clearBuffer;
        private CHAR_INFO[,] _writeBuffer;
        private ConsoleColor[,] _colorClearBuffer;
        private ConsoleColor[,] _writeColorBuffer;

        private COORD _consoleBufferSize;
        private COORD _consoleBufferCoord;
        private SMALL_RECT _consoleRect;

        private Stopwatch _stopwatch = new Stopwatch();
        private Queue<double> _tickTimes = new Queue<double>();
        private double _tickSum = 0f;
        private double _lastTick = 0f;
        private bool _frameCountToggle = false;
        private int _maxFrameSamples = 100;
        private int _totalDrawnObjects = 0;

        public double FastestFrameTime { get; set; } = double.MaxValue;
        public double SlowestFrameTime { get; set; } = double.MinValue;
        public double AverageFrameTime { get; set; } = double.MinValue;

        private GraphicsEngine()
        {
            _handle = CreateFile("CONOUT$", FILE_ACCESS, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
            Console.CursorVisible = false;
            
            _stopwatch.Start();
        }

        public static GraphicsEngine Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GraphicsEngine();
                return _instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void CalculateFrameValues()
        {
            double deltaTime = Instance._stopwatch.Elapsed.TotalSeconds - Instance._lastTick;

            Instance._lastTick = Instance._stopwatch.Elapsed.TotalSeconds;

            Instance._tickSum += deltaTime;

            Instance._tickTimes.Enqueue(deltaTime);
            
            while (Instance._tickTimes.Count > Instance._maxFrameSamples)
            {
                Instance._tickSum -= Instance._tickTimes.Dequeue();
            }

            if (deltaTime > Instance.SlowestFrameTime)
            {
                Instance.SlowestFrameTime = deltaTime;
            }

            else if (deltaTime < Instance.FastestFrameTime)
            {
                Instance.FastestFrameTime = deltaTime;
            }

            Instance.AverageFrameTime = Instance._tickSum / Instance._tickTimes.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetFPS()
        {
            return 1f / Instance.AverageFrameTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetAverageFrameTime()
        {
            return Instance.AverageFrameTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetSlowestFrameTime()
        {
            return Instance.SlowestFrameTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetFastestFrameTime()
        {
            return Instance.FastestFrameTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetTotalObjectsDrawn()
        {
            return Instance._totalDrawnObjects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public static void SetBufferSize(int width, int height)  // 100 * 50 = w * h
        {
            Instance._clearBuffer = new CHAR_INFO[height, width];
            Instance._writeBuffer = new CHAR_INFO[height, width];
            Instance._colorClearBuffer = new ConsoleColor[height, width];
            Instance._writeColorBuffer = new ConsoleColor[height, width];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public static void SetConsoleBufferSize(short width, short height)
        {
            Instance._consoleBufferSize = new COORD(width, height);
            Instance._consoleBufferCoord = new COORD(0, 0);
            Instance._consoleRect = new SMALL_RECT()
            {
                Left = 0,
                Top = 0,
                Right = width,
                Bottom = height
            };
            Console.SetWindowSize(1, 1);
            Console.SetBufferSize(width, height);
            Console.SetWindowSize(width, height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        public static void SetClearBuffer(Cell cell)
        {
            for (int i = 0; i < Instance._clearBuffer.GetLength(0); i++)
            {
                for (int j = 0; j < Instance._clearBuffer.GetLength(1); j++)
                {
                    Instance._clearBuffer[i, j] = cell.CharInfo;
                    Instance._colorClearBuffer[i, j] = cell.BackGround;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ToggleFrameCounter()
        {
            Instance._frameCountToggle = !Instance._frameCountToggle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encoding"></param>
        public static void SetOutputEncoding(Encoding encoding)
        {
            Console.OutputEncoding = encoding;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public static bool Render(Camera camera)
        {
            Array.Copy( Instance._clearBuffer,
                        Instance._writeBuffer,
                        Instance._clearBuffer.Length    );

            Array.Copy( Instance._colorClearBuffer,
                        Instance._writeColorBuffer,
                        Instance._colorClearBuffer.Length   );

            var top =       camera.transform.Position.Y - camera.cameraOffset.Y;
            var bottom =    camera.transform.Position.Y + camera.cameraOffset.Y;
            var left =      camera.transform.Position.X - camera.cameraOffset.X;
            var right =     camera.transform.Position.X + camera.cameraOffset.X;

            Point cameraSpaceCoords;
            Point currentPosition;

            Transform currentParent = null;

            Instance._totalDrawnObjects = 0;

            int coordX = 0;
            int coordY = 0;

            ConsoleColor backgroundColor = ConsoleColor.Black;
            ConsoleColor foregroundColor = ConsoleColor.White;

            var cells = BVH
                            .GetCells(camera.viewFrustrum)
                            .OrderBy(o => o.Layer)
                            .ToArray();

            for (int i = 0; i < cells.Count(); i++)
            {
                currentPosition = cells[i].Transform.Position;

                currentParent = cells[i].Transform.Parent;

                while (currentParent != null)
                {
                    currentPosition = currentPosition.Add(currentParent.Position);

                    currentParent = currentParent.Parent;
                }

                if (    currentPosition.X.InRange(left, right - 1) &&
                        currentPosition.Y.InRange(top, bottom - 1))
                {
                    cameraSpaceCoords = currentPosition.ConvertPositionToCameraSpace(camera.transform.Position.Subtract(camera.cameraOffset));

                    coordX = (int)cameraSpaceCoords.X;
                    coordY = (int)cameraSpaceCoords.Y;

                    if (cells[i].TransparentBackground == TransparentBackground.FALSE)
                    {
                        Instance._writeColorBuffer[ coordX,
                                                    coordY  ] = cells[i].BackGround;
                    }

                    backgroundColor = Instance._writeColorBuffer[   coordX,
                                                                    coordY  ];

                    foregroundColor = cells[i].ForeGround;

                    Instance._writeBuffer[  coordX,
                                            coordY  ].Char = cells[i].CharInfo.Char;

                    Instance._writeBuffer[  coordX,
                                            coordY  ].Attributes = (short)(((int)backgroundColor << 4) + foregroundColor);

                    Instance._totalDrawnObjects += 1;
                }
            }

            CalculateFrameValues();

            return WriteConsoleOutput(  Instance._handle,
                                        Instance._writeBuffer,
                                        Instance._consoleBufferSize,
                                        Instance._consoleBufferCoord,
                                        ref Instance._consoleRect   );
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Exit()
        {
            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = true;
        }
    }
}
