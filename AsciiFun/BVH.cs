﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace AsciiFun
{
    public class BVH
    {
        private static Dictionary<Cell, Node> cellNodes = new Dictionary<Cell, Node>();

        private static BVH instance = null;

        private Node root = null;

        private int ChunkWidth;
        private int ChunkHeight;
        private int WorldWidthInChunks;
        private int WorldHeightInChunks;
        private int WorldWidth;
        private int WorldHeight;

        public static BVH Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BVH();
                }
                return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ChunkWidth"></param>
        /// <param name="ChunkHeight"></param>
        /// <param name="WorldWidthInChunks"></param>
        /// <param name="WorldHeightInChunks"></param>
        private BVH(    int ChunkWidth = 100,
                        int ChunkHeight = 100,
                        int WorldWidthInChunks = 1024,
                        int WorldHeightInChunks = 1024  )
        {

        }

        public static void Init(    int ChunkWidth,
                                    int ChunkHeight,
                                    int WorldWidthInChunks,
                                    int WorldHeightInChunks )
        {
            Instance.ChunkWidth = ChunkWidth;
            Instance.ChunkHeight = ChunkHeight;
            Instance.WorldWidthInChunks = WorldWidthInChunks;
            Instance.WorldHeightInChunks = WorldHeightInChunks;


            Instance.WorldWidth = ChunkWidth * WorldWidthInChunks;
            Instance.WorldHeight = ChunkHeight * WorldHeightInChunks;

            Instance.root = new Node(   null,
                                        0 - Instance.WorldWidth / 2,
                                        0 - Instance.WorldHeight / 2,
                                        Instance.WorldWidth,
                                        Instance.WorldHeight,
                                        ChunkWidth,
                                        ChunkHeight,
                                        false   );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cells"></param>
        public static void AddCells(params Cell[] cells)
        {
            for (int i = 0; i < cells.Count(); i++)
            {
                Instance.root.AddCell(cells[i]);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cells"></param>
        public static void RemoveCells(params Cell[] cells)
        {
            for (int i = 0; i < cells.Count(); i++)
            {
                if (cellNodes.ContainsKey(cells[i]))
                {
                    cellNodes[cells[i]].RemoveCell(cells[i]);
                    cellNodes.Remove(cells[i]);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static List<Cell> GetCells(Rect rect)
        {
            return Instance.root.GetCells(rect);
        }


        public class Node : IComparable<Node>
        {
            public static List<Node> Nodes = new List<Node>();

            private Node parent = null;
            private Rect boundingVolume = Rect.Empty;

            public double Width;
            public double Height;
            public Point TopLeft;

            private double nextWidth;
            private double nextHeight;

            private double ChunkWidth;
            private double ChunkHeight;

            private bool IsLeaf = false;

            private List<Node> nodes = new List<Node>();           

            private List<Cell> cells = new List<Cell>();

            /// <summary>
            /// 
            /// </summary>
            /// <param name="parent"></param>
            /// <param name="X"></param>
            /// <param name="Y"></param>
            /// <param name="Width"></param>
            /// <param name="Height"></param>
            /// <param name="ChunkWidth"></param>
            /// <param name="ChunkHeight"></param>
            public Node(    Node parent,
                            double X,
                            double Y,
                            double Width,
                            double Height,
                            double ChunkWidth,
                            double ChunkHeight,
                            bool IsLeaf )
            {
                this.parent = parent;
                this.TopLeft = new Point(X, Y);
                this.Width = Width;
                this.Height = Height;
                this.ChunkWidth = ChunkWidth;
                this.ChunkHeight = ChunkHeight;
                this.IsLeaf = IsLeaf;

                nextWidth = Width / 2;
                nextHeight = Height / 2;

                boundingVolume = new Rect(  X,
                                            Y,
                                            Width,
                                            Height);

                Nodes.Add(this);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="point"></param>
            /// <returns></returns>
            public bool Contains(Cell cell)
            {
                return boundingVolume.Contains(cell.Transform.Position);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="rect"></param>
            /// <returns></returns>
            public bool Contains(Rect rect)
            {
                return boundingVolume.Contains(rect);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public bool HasChildren()
            {
                return nodes.Count > 0;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="rect"></param>
            /// <returns></returns>
            public bool IntersectsWith(Rect rect)
            {
                return boundingVolume.IntersectsWith(rect);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="rect"></param>
            /// <returns></returns>
            public List<Cell> GetCells(Rect rect)
            {
                List<Cell> overlappingPoints = new List<Cell>();

                if (boundingVolume.IntersectsWith(rect))
                {
                    overlappingPoints.AddRange(cells);

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        overlappingPoints.AddRange(nodes[i].GetCells(rect));
                    }
                }

                return overlappingPoints;
            }


            /// <summary>
            /// 
            /// </summary>
            /// <param name="cell"></param>
            /// <returns></returns>
            public bool AddCell(Cell cell)
            {
                if (this.boundingVolume.Contains(cell.Transform.Position))
                {
                    if (IsLeaf)
                    {
                        BVH.cellNodes[cell] = this;

                        cells.Add(cell);

                        return true;
                    }
                    else
                    {
                        if (!nodes.Any())
                        {
                            if (    nextWidth >= ChunkWidth &&
                                    nextHeight >= ChunkHeight)
                            {
                                bool isLeaf = ( nextWidth == ChunkWidth &&
                                                nextHeight == ChunkHeight   );

                                nodes.Add(new Node( this,
                                                    TopLeft.X + nextWidth,
                                                    TopLeft.Y + nextHeight,
                                                    nextWidth,
                                                    nextHeight,
                                                    ChunkWidth,
                                                    ChunkHeight,
                                                    isLeaf));

                                nodes.Add(new Node( this,
                                                    TopLeft.X + nextWidth,
                                                    TopLeft.Y,
                                                    nextWidth,
                                                    nextHeight,
                                                    ChunkWidth,
                                                    ChunkHeight,
                                                    isLeaf));

                                nodes.Add(new Node( this,
                                                    TopLeft.X,
                                                    TopLeft.Y + nextHeight,
                                                    nextWidth,
                                                    nextHeight,
                                                    ChunkWidth,
                                                    ChunkHeight,
                                                    isLeaf));

                                nodes.Add(new Node( this,
                                                    TopLeft.X,
                                                    TopLeft.Y,
                                                    nextWidth,
                                                    nextHeight,
                                                    ChunkWidth,
                                                    ChunkHeight,
                                                    isLeaf));

                                nodes.Sort();
                            }
                        }


                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].Contains(cell))
                            {
                                if (nodes[i].AddCell(cell) == true)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }

                return false;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="cell"></param>
            public void RemoveCell(Cell cell)
            {
                this.cells.Remove(cell);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public int CompareTo(Node other)
            {
                return TopLeft.LengthSqr().CompareTo(other.TopLeft.LengthSqr());
            }
        }
    }
}