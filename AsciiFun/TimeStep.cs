﻿using System.Diagnostics;

namespace AsciiFun
{
    public class TimeStep
    {
        public double Accumulator { get; private set; } = 0d;

        public double LastTick { get; private set; } = 0d;

        public double CurrentTick { get; private set; } = 0d;

        public double Delta { get; private set; } = 0f;

        public double Step { get; set; } = 1 / 60f;

        private Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// 
        /// </summary>
        public bool Ready
        {
            get
            {
                if (Accumulator >= Step)
                {
                    Accumulator -= Step;
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Enabled"></param>
        public TimeStep(bool Enabled = false)
        {
            if (Enabled == true)
            {
                stopwatch.Start();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Step"></param>
        public void SetStep(double Step)
        {
            if (Step < 0)
            {
                Step = 0;
            }

            this.Step = Step;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            stopwatch.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            stopwatch.Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Tick()
        {
            CurrentTick = stopwatch.Elapsed.TotalSeconds;

            Delta = CurrentTick - LastTick;

            Accumulator += Delta;

            LastTick = CurrentTick;
        }
    }
}
