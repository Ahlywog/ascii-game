﻿using System;
using System.Windows;

namespace AsciiFun.Scripts
{
    public class CircularMotion: Behaviour
    {
        public Point center { get; set; }

        public Point radius { get; set; }

        public double angle { get; set; }

        public double rateOfRotation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="step"></param>
        public override void FixedUpdate(double step)
        {
            angle += rateOfRotation * step;

            if (angle >= 360)
            {
                var difference = 360d - angle;

                angle = difference;
            }

            var radians = angle * Math.PI / 180;

            gameObject.transform.Position = new Point(  center.X + radius.X * Math.Cos(radians),
                                                        center.Y + radius.Y * Math.Sin(radians)    );
        }
    }
}
