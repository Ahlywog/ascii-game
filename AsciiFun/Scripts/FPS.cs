﻿using System;

namespace AsciiFun.Scripts
{
    public class FPS : Behaviour
    {
        public Renderer Renderer { get; set; } = null;

        public Func<double> GetFPS { get; set; } = null;

        public TimeStep TimeStep { get; set; } = null;

        public string StringMask { get; set; } = string.Empty;

        public ConsoleColor BackgroundColor { get; set; }
        public ConsoleColor ForegroundColor { get; set; }
        public TransparentBackground BackgroundTransparency { get; set; }

        private Cell[] _cells;

        /// <summary>
        /// 
        /// </summary>
        public override void Start()
        {
            if (TimeStep == null)
            {
                TimeStep = new TimeStep(true)
                {
                    Step = 1/15f
                };
            }

            if (Renderer == null)
            {
                Renderer = GetComponent<Renderer>();
            }

            _cells = new Cell[StringMask.Length];
            
            for (int i = 0; i < StringMask.Length; i++)
            {
                _cells[i] = new Cell(    CellLayer.UI,
                                        '\0',
                                        i,
                                        0f,
                                        BackgroundColor,
                                        ForegroundColor,
                                        BackgroundTransparency  );

                _cells[i].Transform.Parent = gameObject.transform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="step"></param>
        public override void Update(double step)
        {
            TimeStep.Tick();

            while (TimeStep.Ready)
            {
                Renderer.ClearCells();

                double fps = GetFPS.Invoke();

                string maskedString = fps.ToString(StringMask);

                for (int i = 0; i < maskedString.Length; i++)
                {
                    _cells[i].Character = maskedString[i];
                }

                Renderer.AddCells(_cells);
            }
        }
    }
}
