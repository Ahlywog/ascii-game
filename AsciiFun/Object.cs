﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiFun
{
    public class Object
    {
        protected static List<Object> Objects = new List<Object>();

        private Guid InstanceID = Guid.NewGuid();

        public string Name { get; set; }

        public Object()
        {
            Objects.Add(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Guid GetInstanceID()
        {
            return InstanceID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool Destroy(Object obj)
        {
            return Objects.Remove(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T FindObjectOfType<T>()
        {
            return Objects.Where(x => x is T).Cast<T>().First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T[] FindObjectsOfType<T>()
        {
            return Objects.Where(x => x is T).Cast<T>().ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static T FindObjectByName<T>(string Name)
        {
            return Objects.Where(x => x.Name == Name).Cast<T>().First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static T[] FindObectsByName<T>(string Name)
        {
            return Objects.Where(x => x.Name == Name).Cast<T>().ToArray();
        }
    }
}
