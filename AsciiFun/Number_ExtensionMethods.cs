﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiFun
{
    public static class Number_ExtensionMethods
    {
        public static bool IsPerfectSquare(this int num)
        {
            return Math.Sqrt(num) % 1 == 0;
        }

        public static bool IsPerfectSquare(this long num)
        {
            return Math.Sqrt(num) % 1 == 0;
        }

        public static bool IsEvenNumber(this int num)
        {
            return num % 2 == 0;
        }

        public static bool IsEvenNumber(this long num)
        {
            return num % 2 == 0;
        }

        public static bool InRange(this int num, int lower, int upper)
        {
            return (num >= lower && num <= upper);
        }

        public static bool InRange(this float num, float lower, float upper)
        {
            return (num >= lower && num <= upper);
        }

        public static bool InRange(this double num, double lower, double upper)
        {
            return (num >= lower && num <= upper);
        }

        public static bool IsBetween(this int num, int lower, int upper)
        {
            return (num > lower && num < upper);
        }

        public static bool IsBetween(this float num, float lower, float upper)
        {
            return (num > lower && num < upper);
        }

        public static bool IsBetween(this double num, double lower, double upper)
        {
            return (num > lower && num < upper);
        }
    }
}
