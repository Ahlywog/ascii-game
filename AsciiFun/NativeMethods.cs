﻿using Microsoft.Win32.SafeHandles;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace AsciiFun
{
    public class NativeMethods
    {
        // http://stackoverflow.com/questions/7937256/orange-text-color-in-c-sharp-console-application

        protected const int STD_OUTPUT_HANDLE = -11;

        protected const uint FILE_ACCESS = 0x40000000;

        public const int MF_BYCOMMAND = 0x00000000;
        public const int SC_CLOSE = 0xF060;
        public const int SC_MINIMIZE = 0xF020;
        public const int SC_MAXIMIZE = 0xF030;


        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        protected static extern SafeFileHandle CreateFile(  string fileName,
                                                            [MarshalAs(UnmanagedType.U4)] uint fileAccess,
                                                            [MarshalAs(UnmanagedType.U4)] uint fileShare,
                                                            IntPtr securityAttributes,
                                                            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
                                                            [MarshalAs(UnmanagedType.U4)] int flags,
                                                            IntPtr template);


        [DllImport("kernel32.dll", EntryPoint = "WriteConsoleOutputW", CharSet = CharSet.Unicode, SetLastError = true)]
        protected static extern bool WriteConsoleOutput(    SafeFileHandle hConsoleOutput,
                                                            CHAR_INFO[,] lpBuffer, //[MarshalAs(UnmanagedType.LPArray), In]
                                                            COORD dwBufferSize,
                                                            COORD dwBufferCoord,
                                                            ref SMALL_RECT lpWriteRegion);

        [DllImport("kernel32.dll", SetLastError = true)]
        protected static extern bool GetConsoleScreenBufferInfoEx(  IntPtr hConsoleOutput,
                                                                    ref CONSOLE_SCREEN_BUFFER_INFO_EX csbe);

        [DllImport("kernel32.dll", SetLastError = true)]
        protected static extern bool SetConsoleScreenBufferInfoEx(  IntPtr hConsoleOutput, 
                                                                    ref CONSOLE_SCREEN_BUFFER_INFO_EX csbe);
    }
}
